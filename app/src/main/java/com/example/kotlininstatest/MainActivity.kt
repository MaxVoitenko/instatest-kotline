package com.example.kotlininstatest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.example.kotlininstatest.Model.Post
import com.example.kotlininstatest.Providers.DataProvider
import com.example.kotlininstatest.Ui.PostAdapter
import java.util.Collections.list

class MainActivity : AppCompatActivity(), PostAdapter.Callback {
    override fun clickAdapter(v: View) {
        when (v.id) {
            R.id.sendBtn -> Toast.makeText(this, "send", Toast.LENGTH_SHORT).show()
            R.id.commentBtn -> Toast.makeText(this, "comment", Toast.LENGTH_SHORT).show()
            R.id.likeBtn -> Toast.makeText(this, "like", Toast.LENGTH_SHORT).show()
            R.id.menuBtn -> Toast.makeText(this, "menu", Toast.LENGTH_SHORT).show()
            R.id.saveBtn -> Toast.makeText(this, "save pic", Toast.LENGTH_SHORT).show()
        }
    }
    var items: List<Post>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView:RecyclerView = findViewById(R.id.recyclerView)

        val netPostProvider = DataProvider(this)
        items = netPostProvider.getAll()
        val adapeter = PostAdapter(items!!, this)
        adapeter.setOnClickAdapter(this)
        recyclerView.adapter = adapeter
        adapeter.notifyDataSetChanged()
    }
}
