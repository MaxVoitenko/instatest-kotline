package com.example.kotlininstatest.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Post (
    @SerializedName("name") val name: String,
    @SerializedName("city") val city: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("array") val array: List<String>,
    @SerializedName("linkProfilePic") val linkProfilePic: String,
    @SerializedName("linkPostPic") val linkPostPic: String
)