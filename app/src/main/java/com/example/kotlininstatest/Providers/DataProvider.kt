package com.example.kotlininstatest.Providers

import android.content.Context
import com.example.kotlininstatest.Model.Post
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.annotations.Nullable
import java.io.FileNotFoundException
import java.io.IOException

class DataProvider(val context: Context) {

    fun getAll(): List<Post> {
        val gson = Gson()
        val collectionType = object : TypeToken<List<Post>>() {}.type
        return Gson().fromJson<Any>(getJsonFile(), collectionType) as List<Post>
    }

    private fun getJsonFile(): String {
        var text = "-1"
        try {
            val istream = context.getAssets().open("data.txt")
            val size = istream.available()
            val buffer = ByteArray(size)
            istream.read(buffer)
            text = String(buffer)
            istream.close()
        } catch (e: FileNotFoundException) {e.printStackTrace()
        } catch (e: IOException){e.printStackTrace()} finally {
            return text
        }
    }
}