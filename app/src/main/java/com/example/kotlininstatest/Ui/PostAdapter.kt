package com.example.kotlininstatest.Ui

import android.content.Context
import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.example.kotlininstatest.Model.Post
import com.example.kotlininstatest.R
import com.github.siyamed.shapeimageview.RoundedImageView
import kotlinx.android.synthetic.main.item_recycler.view.*
import java.text.MessageFormat

class PostAdapter(val items: List<Post>, val context: Context) : RecyclerView.Adapter<PostAdapter.Holder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler, parent, false) as View
        return  Holder(view)}

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val post = items[holder.adapterPosition]

        holder.nameTxt.setText(post.name)
        holder.cityTxt.setText(post.city)
        holder.commentTxt.setText(post.comment)
        val likers = post.array
        if (likers.size >= 2) {
            holder.likeByTxt.text = MessageFormat.format("liked by {0}, {1}, {2} and {3} other", likers.get(0), likers.get(1),
                    likers.get(2), (likers.size - 2).toString())
        } else {
            val idList = likers.toString()
            val csv = idList.substring(1, idList.length - 1).replace(", ", ",")
            holder.likeByTxt.setText(csv)
        }
        var imageId = context.getResources().getIdentifier(post.linkPostPic, "drawable", context.getPackageName())
        holder.mainImage.setImageResource(imageId)
        imageId = context.getResources().getIdentifier(post.linkProfilePic, "drawable", context.getPackageName())
        holder.profileImage.setImageResource(imageId)

        val clickListener = View.OnClickListener { v -> callbackClick!!.clickAdapter(v) }
        holder.sendBtn.setOnClickListener(clickListener)
        holder.commentBtn.setOnClickListener(clickListener)
        holder.likeBtn.setOnClickListener(clickListener)
        holder.menuBtn.setOnClickListener(clickListener)
        holder.saveBtn.setOnClickListener(clickListener)
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTxt = itemView.nameTxt
        val cityTxt = itemView.cityTxt
        val mainImage = itemView.mainImage
        val likeByTxt = itemView.likeByTxt
        val commentTxt = itemView.commentTxt
        val profileImage = itemView.profileImage
        val menuBtn = itemView.menuBtn
        val likeBtn = itemView.likeBtn
        val commentBtn = itemView.commentBtn
        val saveBtn = itemView.saveBtn
        val sendBtn = itemView.sendBtn
    }

    private var callbackClick: Callback? = null
    interface Callback {fun clickAdapter(v: View)}
    fun setOnClickAdapter(callback: Callback){this.callbackClick = callback }
}
